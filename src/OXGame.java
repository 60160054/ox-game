import java.util.*;

public class OXGame {

	Scanner kb = new Scanner(System.in);

	static char[][] Board = {   { ' ', '1', '2', '3' }, 
								{ '1', '-', '-', '-' }, 
								{ '2', '-', '-', '-' },
								{ '3', '-', '-', '-' }, };
	static char turn = 'X';
	static boolean Stop = true;
	static String  result;
	static void printWelcome() {
		System.out.println("Welcome to OX Game");
	}

	static void printBoard() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(Board[i][j] + " ");
			}
			System.out.println();
		}
	}

	static void SwitchTurn() {
		if (turn == 'X') {
			turn = 'O';
		} else if (turn == 'O') {
			turn = 'X';
		}
	}

	static void printTurn() {
		System.out.println("Turn " + turn);
	}

	static void input() {
		Scanner kb = new Scanner(System.in);
		int R;
		int C;
		while (true) {
			System.out.println("Please Input R,C : ");
			R = kb.nextInt();
			C = kb.nextInt();
			if (R < 1 || R > 3) {
				System.out.println("Error, Please Input again");
				continue;
			} else if (C < 1 || C > 3) {
				System.out.println("Error, Please Input again");
				continue;

			} else {
				break;
			}
		}
		Board[C][R] = turn;
	}

	static void CheckWin() {

		if(Board[1][1] == turn&&Board[1][2] == turn&&Board[1][3] == turn 
				|| Board[2][1] == turn&&Board[2][2] == turn&&Board[2][3] == turn
				|| Board[3][1] == turn&&Board[3][2] == turn&&Board[3][3]==turn ) {
			Stop = false;
			result = "Win";
		}else if(Board[1][1] == turn&&Board[2][1] == turn&&Board[3][1]== turn 
				|| Board[1][2] == turn&&Board[2][2] == turn&&Board[3][2] == turn 
				|| Board[1][3] == turn&&Board[2][3] == turn&&Board[3][3]== turn) {
			Stop = false;
			result = "Win";
		}else if(Board[1][1] == turn&&Board[2][2] == turn&&Board[3][3]== turn 
				|| Board[1][3] == turn&&Board[2][2] == turn&&Board[3][1] == turn ) {
			Stop = false;
			result = "Win";
		}else if(Board[1][1] == '-'|| Board[1][2] == '-'||Board[1][3] == '-' 
				|| Board[2][1] == '-' ||Board[2][2] == '-'||Board[2][3] == '-'
				|| Board[3][1] == '-'||Board[3][2] == '-'||Board[3][3]=='-' ) {
					Stop = true;
				}else {
					Stop = false;
					result ="Draw";
			
		}
	}
	static void printWin() {
		if(result == "Win") {
			System.out.println(turn+" "+result);
		}else {
			System.out.println(result);
		}
		System.out.println("Bye");
	}

	public static void main(String[] args) {
		printWelcome();
		while (Stop) {
			printBoard();
			printTurn();
			input();
			CheckWin();
			if(Stop == true) {
				SwitchTurn();
			}
		}
		printBoard();
		printWin();

	}
}
